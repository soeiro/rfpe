# Charte fondatrice

## Contexte

La structuration d'un réseau national en pharmacoépidémiologie académique, et constitutivement indépendant de l'industrie comme des institutions, a été envisagée dès 2006 dans le [livre blanc de pharmacologie médicale](https://sfpt-fr.org/images/livreblanc_pharmacologiemedicale.pdf) et repris en 2013 dans un [rapport destiné au Ministre de la Santé](https://www.vie-publique.fr/rapport/33483-rapport-sur-la-surveillance-et-la-promotion-du-bon-usage-du-medicament-e). Cette structuration apparaît désormais indispensable pour répondre aux enjeux de l'évaluation des médicaments en situation réelle de soins.

Une première étape a eu lieu en 2019 avec la création du [groupe de travail Pharmacoépidémiologie](https://sfpt-fr.org/groupes-de-travail/pharmacoepidemiologie) de la [Société Française de Pharmacologie et de Thérapeutique](https://sfpt-fr.org/). Sous l'impulsion de plusieurs membres de ce groupe de travail, la structuration d'un réseau national de pharmacoépidémiologie, nommé *French Pharmacoepidemiology Initiative*, a été lancée en 2023. Ce document est la charte de la *French Pharmacoepidemiology Initiative*.

## Expertise

La *French Pharmacoepidemiology Initiative* présente une expertise sur les trois dimensions de la pharmacoépidémiologie : l'évaluation de l'utilisation, de l'effectivité et de la sécurité des médicaments en situation réelle de soins. Elle propose une offre de réponse indépendante à la demande grandissante d'études d'évaluation des médicaments en vie réelle avec un haut niveau méthodologique et technique, impliquant l'utilisation secondaire de données médico-administratives, de données hospitalières ou la collecte de données *ad hoc*.

Par son positionnement dans l'écosystème de l'évaluation du médicament en France, la *French Pharmacoepidemiology Initiative* permettra d'optimiser l'intégration de ces études aux travaux issus des autres domaines d'évaluation du médicament (recherche clinique, pharmacovigilance, addictovigilance, suivi thérapeutique pharmacologique, etc.) dans une perspective d'aide à la décision en santé et de communication autour des bénéfices et des risques des médicaments en population.

## Missions

La *French Pharmacoepidemiology Initiative* répondra à plusieurs missions :

- Conduire des projets de recherche et d'évaluation collaboratifs en pharmacoépidémiologie, y compris dans des domaines d'expertise spécifiques ou des délais contraints, en mutualisant les expertises (pharmacologique, épidémiologique, méthodologique et technique) et les accès aux données (SNDS, entrepôt de données, cohorte *ad hoc*) ;
- Contribuer au partage d'expériences des membres du réseau par le biais de dispositifs de mobilité et de circuits de mentorat ;
- Contribuer à la formation théorique en pharmacoépidémiologie et à l'utilisation secondaire de données pour l'évaluation pharmacoépidémiologique du médicament ;
- Améliorer, *in fine*, la visibilité et l'attractivité de la recherche académique en pharmacoépidémiologie en France.

L'ensemble de ces missions s'inscrit dans une démarche de [science ouverte](https://www.ouvrirlascience.fr/).

## Composition

Afin d'assurer la complémentarité des compétences au sein du réseau, la *French Pharmacoepidemiology Initiative* est ouverte à toute personne issue de la communauté pharmacologique, ou entretenant des interactions fortes avec elle, et impliquée dans la recherche publique sur l'évaluation des médicaments en vie réelle. Cela concerne par conséquent pharmacologues, épidémiologistes, cliniciens, biostatisticiens, ingénieurs en informatique médicale, etc., œuvrant dans ce champ de recherche.

## Organisation et architecture

La *French Pharmacoepidemiology Initiative* s'appuie sur la recherche académique en pharmacoépidémiologie, essentiellement implantée au sein des centres hospitaliers universitaires.

L'objectif ultime est que tous les centres contribuent au réseau avec au moins un expert en pharmacoépidémiologie (ou un binôme constitué d'un expert en pharmacologie et d'un expert en épidémiologie) et un analyste de données formé à l'utilisation secondaires des données de santé (SNDS, entrepôt de données, etc.). Ce maillage territorial offre la configuration idéale pour permettre des collaborations avec des institutions nationales (ANSM, HAS, Santé Publique France, sociétés savantes) comme avec des agences régionales de pilotage sanitaire (pour répondre à des demandes de quantification et caractérisation locale de mésusage dans une perspective d'intervention par exemple). Dans un premier temps, une organisation de type réseau fédéré d'expertises relié par des *hubs* techniques de support pour la réalisation pratique des études peut être envisagée pour diminuer le risque d'isolement des chercheurs et des ingénieurs en charge de la gestion et de l'analyse des données.
