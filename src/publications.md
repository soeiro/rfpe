# Publications du réseau

## 2024

- Soeiro T, Allouchery M, Bene J, Bezin J, Dolladille C, Faillie JL, Grimaldi L, Kaguelidou F, Khouri C, Lafaurie M, Largeau B, Montastruc F, Morin L, Scailteux LM, Pariente A; SFPT Pharmacoepidemiology Working Group. Shaping the future of pharmacoepidemiology in France: Recommendations from the SFPT Pharmacoepidemiology Working Group. Therapie. 2024 Dec 14:S0040-5957(24)00213-0. doi: [10.1016/j.therap.2024.12.005](https://doi.org/10.1016/j.therap.2024.12.005). Epub ahead of print. PMID: [39706774](https://pubmed.ncbi.nlm.nih.gov/39706774/).

## 2023

- de Germay S, Conte C, Micallef J, Bouquet E, Chouchana L, Lafaurie M, Pariente A; Pharmacoepidemiological group of the French Society of Pharmacology and Therapeutics (SFPT). Performing pharmacoepidemiological studies using the French health insurance data warehouse (SNDS): How to translate guidelines into practice. Therapie. 2023 Nov-Dec;78(6):679-689. doi: [10.1016/j.therap.2023.01.009](https://doi.org/10.1016/j.therap.2023.01.009). Epub 2023 Jan 28. PMID: [36841656](https://pubmed.ncbi.nlm.nih.gov/36841656/).

## 2022

- Fiches de Lecture Critique d'Article en Pharmacoépidémiologie & Pharmacovigilance, [publiées sur le site du Collège National de Pharmacologie Médicale](https://pharmacomedicale.org/les-formations/fiches-de-lca-en-pharmacoepidemiologie)
