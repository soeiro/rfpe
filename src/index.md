Bienvenue sur le site de la *French Pharmacoepidemiology Initiative*, le réseau national fédérant les équipes académiques de recherche en pharmacoépidémiologie en France.

# Actualités

## 2024

- 8 octobre 2024 : *Journal club* sur l'article [*The application of lag times in cancer pharmacoepidemiology: a narrative review*](https://doi.org/10.1016/j.annepidem.2023.05.004) (Hicks et al. 2023) présenté par Lucie-Marie Scailteux (Université de Rennes, CHU de Rennes) et animé par Lucas Morin (Inserm U1018) et François Montastruc (Université de Toulouse, CHU de Toulouse).

<figure>
  <img src="img/20241008.png" alt="Journal club octobre 2024">
</figure>

- 10 juin 2024 : Réunion annuelle du Réseau Français de Pharmacoépidémiologie organisé à Tours en ouverture du [congrés annuel de la SFPT](https://www.congres-sfpt.fr/).

<figure>
  <img src="img/20240610.png" alt="Workshop 2024">
</figure>

- 4 juin 2024 : *Journal club* sur l'article [*The Use of Active Comparators in Self-Controlled Designs*](https://doi.org/10.1093/aje/kwab110) (Hallas et al. 2021) présenté par Lucas Morin (Inserm U1018) et animé par François Montastruc (Université de Toulouse, CHU de Toulouse).

## 2023

- 12-13 octobre 2023 : *Workshop* Pharmacoépi organisé à Bordeaux par [DRUGS-SAFEr](https://drugssafer.fr/) en vue de la structuration d'un réseau national de pharmacoépidémiologie.

<figure>
  <img src="img/20231012-1.png" alt="Workshop 2023">
  <img src="img/20231012-2.png" alt="Workshop 2023">
</figure>
