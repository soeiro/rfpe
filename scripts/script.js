// functions --------------------------------------------------------------

// append the text of an element to another one
// setElm: an element whose text is to be set
// getElm: an element whose text is to be get
const appendText = (setElm, getElm) => {
  const text = ` | ${getElm.textContent}`;
  setElm.textContent += text;
};

// filter a table based on an input form
// evt: an event
// trs: a NodeList of table rows
const filterTable = (evt, trs) => {
  const regex = new RegExp(evt.target.value, "i");
  trs.forEach(tr => {
    const cells = Array.from(tr.cells);
    const isFound = cells.some(cell => regex.test(cell.textContent));
    tr.style.display = isFound ? "" : "none";
  });
};

// init -------------------------------------------------------------------

const title = document.querySelector("title");
const h1 = document.querySelector("h1");
appendText(title, h1);

if (window.location.pathname === "/annuaire.html") {
  const input = document.querySelector("input");
  const contacts = document.querySelectorAll("table > tbody > tr");
  input.addEventListener("keyup", evt => filterTable(evt, contacts), false);
}
